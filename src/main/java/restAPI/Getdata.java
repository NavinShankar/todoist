package restAPI;

import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;

public class Getdata {
	
	@Test
	public void testresponsecode(String Url) {
		
		Response resp=RestAssured.get("url");
		int code=resp.getStatusCode();
		System.out.println("Status code is"+code);
		Assert.assertEquals(code, 200);
	}
	
	public void testbody(String Url) {
		
		Response resp=RestAssured.get("url");
		String data=resp.asString();
		System.out.println("Data is"+data);
		
	}
	

}
