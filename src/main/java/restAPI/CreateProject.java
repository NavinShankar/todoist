package restAPI;

import org.json.simple.JSONObject;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class CreateProject {
	
	WebDriver driver;
	
	public CreateProject() {
	
	}
	
	public void createproject() {
		
		RequestSpecification request= RestAssured.given();
		request.header("Content-Type", "application/json");
		request.header("access_token", "9860396d3ff1b74129b321e21118822a2407f771");
		request.header("token_type","Bearer");
		
		JSONObject json=new JSONObject();
		json.put("name" , "Test Project");
	
		request.body(json.toJSONString());
				
		Response response=request.post("https://beta.todoist.com/API/v8/projects");
		int code=response.getStatusCode();
		Assert.assertEquals(code, 201);
		}
	

}
