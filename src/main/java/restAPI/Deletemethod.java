package restAPI;

import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class Deletemethod {
	
	@Test
	public void test1(String URL) {
		
		RequestSpecification request= RestAssured.given();
		
		Response response=request.delete(URL);
		int code=response.getStatusCode();
		Assert.assertEquals(code, 200);
	}

}
