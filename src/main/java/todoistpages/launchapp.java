package todoistpages;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

public class launchapp {
	
	WebDriver driver;
	
	//Run appium node server and appium server then emulator is launched before proceeding with below steps
	public launchapp(WebDriver driver) {
		this.driver=driver;
	}
	
	public void startapp() throws MalformedURLException {
		
	
		 // Created object of DesiredCapabilities class.
		  DesiredCapabilities capabilities = new DesiredCapabilities();
		  
		// Set android deviceName desired capability. Set it Android Emulator.
		  capabilities.setCapability("deviceName", "Android Emulator");
		  
		  // Set browserName desired capability. set as android (needed if in browser cases)
		  //capabilities.setCapability("browserName", "Android");
		  
		// Set android platformVersion desired capability. Set your emulator's android version.
		  capabilities.setCapability("platformVersion", "6.0");
		  
		// Set android platformName desired capability. It's Android in our case here.
		  capabilities.setCapability("platformName", "Android");
		  
		  // Set your application's appPackage for todoist
		  capabilities.setCapability("appPackage", "com.android.todoist");
		 
		  // Set your application's appActivity for todoist
		  capabilities.setCapability("appActivity", "com.android.todoist.todoist");
		  
		// Created object of RemoteWebDriver will all set capabilities.
		  // Set appium server address and port number in URL string.
		  
		  driver = new RemoteWebDriver(new URL("http://0.0.0.0:4723/wd/hub"), capabilities);
		  driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		    
	}
	
}
