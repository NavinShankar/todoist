package todoistpages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class homepage {
	WebDriver driver;
	
	By login=By.xpath("/html/body/div[1]/nav/div/div/div/div[2]/ul/li[5]/a");
	By email=By.name("email");
	By password=By.name("password");
	By loginbtn=By.xpath("//*[@id=\"login_form\"]/a");
	By forgotpassword=By.xpath("//*[@id=\"login_form\"]/div[2]/p[1]/a");
	By resetemail=By.xpath("//*[@id=\"email\"]");
	By resetpassword=By.xpath("//*[@id=\"reset_password_form\"]/a");
	
	public homepage(WebDriver driver) {
		this.driver=driver;
	}
	
	public void clickLogin() {
		driver.findElement(login).click();
	}
	
	public void typeemail(String eml) {
		driver.findElement(email).sendKeys(eml);
	}
	
	public void typepassword(String pwd) {
		driver.findElement(password).sendKeys(pwd);
	}
	
	public void loginbtn() {
		driver.findElement(loginbtn).click();
	}
	
	public void clickforgotpassword() {
		driver.findElement(forgotpassword).click();
	}
	
	public void typeresetemail(String rsteml) {
		driver.findElement(resetemail).sendKeys(rsteml);
	}
	
	public void clickresetpassword() {
		driver.findElement(resetpassword).click();
	}

}
