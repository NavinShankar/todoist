package todoistpages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class userpage {
	WebDriver driver;
	
	//These elements can be captured using UIautomationviewer that comes in android SDK or web elements in web app
	By menubtn=By.xpath("//*[@id=\"top_bar_inner\"]/a");
	By quicktask=By.xpath("//*[@id=\"quick_add_task_holder\"]/span/svg/g/g/path");
	By Addtasklink=By.linkText("add task");
	By addtaskbtn=By.xpath("//*[@id=\"empty-state-holder\"]/div/button");
	By tasktext=By.xpath("//*[@id=\"editor\"]/div/div[1]/ul/li[11]/form/table[1]/tbody/tr/td/table/tbody/tr/td[1]/div");
	By duedate=By.xpath("//*[@id=\"editor\"]/div/div[1]/ul/li[11]/form/table[1]/tbody/tr/td/table/tbody/tr/td[2]/input");
	By cancelbtn=By.xpath("//*[@id=\"editor\"]/div/div[1]/ul/li[11]/form/table[2]/tbody/tr/td[1]/a[2]");
	By confirmtaskbtn=By.xpath("//*[@id=\"editor\"]/div/div[1]/ul/li[11]/form/table[2]/tbody/tr/td[1]/a[1]");
	By projects=By.xpath("//*[@id=\"list_holder\"]/div[2]/div[1]/div[2]/h5");
	By projectsym=By.xpath("//*[@id=\"list_holder\"]/div[2]/div[1]/div[2]/svg");
	By projectlink=By.linkText("Add Project");
	By projectname=By.xpath("//*[@id=\"projects_list\"]/li[2]/form/table[1]/tbody/tr/td[1]/table/tbody/tr/td/div[2]");
	By addprojectbtn=By.xpath("//*[@id=\"projects_list\"]/li[2]/form/table[2]/tbody/tr/td[1]/a[1]");
	By projectcancelbtn=By.xpath("//*[@id=\"projects_list\"]/li[2]/form/table[2]/tbody/tr/td[1]/a[2]");
	By currentproject=By.xpath("//*[@id=\"projects_list\"]/li[2]");
	By projecttask=By.xpath("//*[@id=\"editor\"]/div/div[2]/a");
	By closetask=By.xpath("//*[@id=\"item_3069923879\"]/table/tbody/tr/td[1]");
	By completedtask=By.xpath("//*[@id=\"editor\"]/div[1]/div[2]/div/span/svg");
	
	
	
	public userpage(WebDriver driver) {
		this.driver=driver;
	}
	
	public void clickmenubutton() {
		driver.findElement(menubtn).click();
	}
	
	public void clickexistproject() {
		driver.findElement(currentproject).click();
	}
	public void clickprojects() {
		driver.findElement(projects).click();
	}

	public void clickaddnewtask() {
		driver.findElement(projecttask).click();
	}
	
	public void entertask(String task) {
		driver.findElement(tasktext).sendKeys(task);	
	}
	
	public void enterdate(String date) {
		driver.findElement(duedate).sendKeys(date);
	}
	
	public void clickaddtaskbtn() {
		driver.findElement(confirmtaskbtn).click();
	}
	
	public void clickclosetask() {
		driver.findElement(closetask).click();
	}
	
	public void clickcompletedtask() {
		driver.findElement(completedtask).click();
	}
	
}
