package testcases;

import static org.testng.Assert.assertTrue;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import todoistpages.homepage;
import todoistpages.launchapp;
import todoistpages.userpage;

public class Testcase3 {
	
	WebDriver driver;
	
	@Test
	public void completetask() throws MalformedURLException {
		
		launchapp action=new launchapp(driver);
		action.startapp();
		
		homepage act=new homepage(driver);
		act.clickLogin();
		act.typeemail("sportingstar15@gmail.com");
		act.typepassword("Christ15");
		act.loginbtn();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		userpage act1=new userpage(driver);
		act1.clickmenubutton();
		act1.clickprojects();
		act1.clickexistproject();
		//clicks on the checkbox for closing an existing task
		act1.clickclosetask();
		act1.clickcompletedtask();
		
		String closedtask=driver.findElement(By.xpath("//*[@id=\"editor\"]/div[1]/div[3]")).getText();
		assertTrue(closedtask.contains("Test Project"));
				
	}
	
	@Test
	public void reopentask() {
		
		RequestSpecification request= RestAssured.given();
		request.header("Authorization", "Bearer %s % 9860396d3ff1b74129b321e21118822a2407f771");
		request.header("access_token", "9860396d3ff1b74129b321e21118822a2407f771");
		request.header("token_type","Bearer");
		
		Response response=request.post("https://beta.todoist.com/API/v8/tasks/2206209031/reopen");
		int code=response.getStatusCode();
		Assert.assertEquals(code, 204);
	}
	
	@Test
	public void verifytaskappears() throws MalformedURLException {
		launchapp action=new launchapp(driver);
		action.startapp();
		
		homepage act=new homepage(driver);
		act.clickLogin();
		act.typeemail("sportingstar15@gmail.com");
		act.typepassword("Christ15");
		act.loginbtn();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		userpage act1=new userpage(driver);
		act1.clickmenubutton();
		act1.clickprojects();
		act1.clickexistproject();
		
		String task=driver.findElement(By.xpath("//*[@id=\"item_3070953528\"]")).getText();
		assertTrue(task.contains("Test Project"));
		
	}

}
