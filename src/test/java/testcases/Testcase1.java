package testcases;

import static org.testng.Assert.assertTrue;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

import org.json.simple.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import todoistpages.homepage;
import todoistpages.launchapp;
import todoistpages.userpage;

public class Testcase1 {
	WebDriver driver;
	
	@Test
	public void createprojectAPI() {

		//RestAssured library used for restful API automation
		RequestSpecification request= RestAssured.given();
		request.header("Content-Type", "application/json");
		request.header("access_token", "9860396d3ff1b74129b321e21118822a2407f771");
		request.header("token_type","Bearer");
		
		JSONObject json=new JSONObject();
		json.put("name" , "Test Project");
	
		request.body(json.toJSONString());
				
		Response response=request.post("https://beta.todoist.com/API/v8/projects");
		int code=response.getStatusCode();
		Assert.assertEquals(code, 201);
		}
	
	@Test
	public void verifyproject() throws MalformedURLException {
		
		//code to launch app in the emulator, taken from the todoistpages package
		launchapp action=new launchapp(driver);
		action.startapp();
		
		//code to login todoist app taken from the homepage class in todoist package
		homepage act=new homepage(driver);
		
		act.clickLogin();
		act.typeemail("sportingstar15@gmail.com");
		act.typepassword("Christ15");
		act.loginbtn();
		
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		
		//object created for userpage class to access elements in the user page
		userpage act1=new userpage(driver);
		act1.clickmenubutton();
		act1.clickprojects();
		
		String Actual=driver.findElement(By.xpath("//*[@id=\"list_holder\"]/div[2]/div[2]/div")).getText();
		
		assertTrue(Actual.contains("Test Project"));
			
	}
}
