package testcases;

import static org.testng.Assert.assertTrue;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import todoistpages.homepage;
import todoistpages.launchapp;
import todoistpages.userpage;

public class Testcase2 {
	
	WebDriver driver;
	
	@Test
	public void createtask() throws MalformedURLException {
		
		launchapp action=new launchapp(driver);
		action.startapp();
		
		homepage act=new homepage(driver);
		act.clickLogin();
		act.typeemail("sportingstar15@gmail.com");
		act.typepassword("Christ15");
		act.loginbtn();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		userpage act1=new userpage(driver);
		act1.clickmenubutton();
		act1.clickprojects();
		act1.clickexistproject();
		act1.clickaddnewtask();
		act1.entertask("Test task");
		act1.enterdate("02 March");
		act1.clickaddtaskbtn();
		
		String actualtask=driver.findElement(By.xpath("//*[@id=\"editor\"]/div/div[1]/ul")).getText();
		assertTrue(actualtask.contains("Test Project"));	
		
	}
	
	@Test
	public void verifytaskAPI() {
		RequestSpecification request= RestAssured.given();
		request.header("Content-Type", "application/json");
		request.header("access_token", "9860396d3ff1b74129b321e21118822a2407f771");
		request.header("token_type","Bearer");
		
		
		Response resp=RestAssured.get("https://beta.todoist.com/API/v8/tasks/2206209031");
		int code=resp.getStatusCode();
		System.out.println("Status code is"+code);
		Assert.assertEquals(code, 200);
		
	}

}
