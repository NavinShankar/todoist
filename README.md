Please use the below configuration and specifications to run the project using the test scripts written in Java code.

The below pre-requistes should be done first in order to proceed with automation test for Todoist app:

Run the Appium server desktop to get the mobile app testing in progress.
Node.JS should be installed along with Java in the working system.
Download And Install Java(JDK) In Windows.
Download And Install Android SDK In Windows.
Set Path Environment Variables For SDK In Windows.
Steps To Install ADT Plugin To Use SDK In Eclipse.
Configure Project In Eclipse For Appium.
Create And Start An Android Virtual Device(Emulator).
Install/Uninstall App In Android Emulator (AVD).
Use the AVD manager to install/uninstall the Apk file in the emulator
GenyMotion or Android SDK used to create Andriod virtual Devices (AVD)
Eclipse version 2018-09 used as IDE to run Java codes.
TestNG framework with Maven Dependencies.

